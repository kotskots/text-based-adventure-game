import java.io.*;
import java.util.*;


public class Commands {
    //public String position = " ";
//    static LimitedHashSet<String> inventory = new LimitedHashSet<>();
    HashSet<String> inventory = new HashSet<>();
    public String LastLook = " ";
    public boolean safeOpen = false;
    public boolean airwayOpen = false;
    public boolean flashlightAvailable = false;
    public boolean hasUsedLadder = false;
    public boolean hasUsedScrewdriver = false;
    public boolean doorUnlocked = false;
    public boolean doorOpen = false;

    public String Drop(String input){
        if (input.equals("drop screwdriver"))
        {   if(inventory.contains("screwdriver")){
            System.out.println("You just removed the screwdriver from your inventory.");
            inventory.remove("screwdriver");
            System.out.println(inventory);}
            else
            System.out.println("You don't have any screwdrivers in your inventory.");
        }
        else if (input.equals("drop ladder")){
            if (inventory.contains("ladder")){
                System.out.println("You just removed the ladder from your inventory");
                inventory.remove("ladder");}
            else
                System.out.println("You don't have any ladders in your inventory.");
        }else if (input.equals("drop flashlight")){
            if (inventory.contains("flashlight")){
                System.out.println("You just removed the flashlight from your inventory");
                inventory.remove("flashlight");}
            else
                System.out.println("You don't have any flashlights in your inventory.");
        }else if (input.equals("drop bat")){
            if (inventory.contains("bat")){
                System.out.println("You just removed the bat from your inventory");
                inventory.remove("bat");}
            else
                System.out.println("You don't have any bats in your inventory.");
        }else if (input.equals("drop shield")){
            if (inventory.contains("shield")){
                System.out.println("You just removed the shield from your inventory");
                inventory.remove("shield");}
            else
                System.out.println("You don't have any shields in your inventory.");
        }else if (input.equals("drop knives")){
            if (inventory.contains("knives")){
                System.out.println("You just removed the knives from your inventory");
                inventory.remove("knives");}
            else
                System.out.println("You don't have any knives in your inventory.");
        }else if (input.equals("drop Katana sword")){
            if (inventory.contains("Katana sword")){
                System.out.println("You just removed the Katana sword from your inventory");
                inventory.remove("Katana sword");}
            else
                System.out.println("You don't have any Katana swords in your inventory.");
        }else if (input.equals("drop helmet")){
            if (inventory.contains("helmet")){
                System.out.println("You just removed the ladder from your inventory");
                inventory.remove("ladder");}
            else
                System.out.println("You don't have any ladders in your inventory.");
        }else if (input.equals("drop armor")){
            if (inventory.contains("armor")){
                System.out.println("You just removed the armor from your inventory");
                inventory.remove("armor");}
            else
                System.out.println("You don't have any armors in your inventory.");
        }else if (input.equals("drop potion")){
            if (inventory.contains("potion")){
                System.out.println("You just removed the potion from your inventory");
                inventory.remove("potion");}
            else
                System.out.println("You don't have any potions in your inventory.");
        }else if (input.equals("drop arc")){
            if (inventory.contains("arc")){
                System.out.println("You just removed the arc from your inventory");
                inventory.remove("arc");}
            else
                System.out.println("You don't have any arcs in your inventory.");
            }
        else {
            System.out.println("That's not a sentence I can understand.");
        }
        return LastLook;
    }

    public void Use(String input, String position, String LastLook)
    {
        if(input.equals("use flashlight") && inventory.contains("flashlight"))
        {   if (inventory.contains("batteries")){
            flashlightAvailable = true;
            System.out.println("Flashlight is on.Now you are able to see into the Warehouse.");
        inventory.remove("batteries");}
            else{
            System.out.println("You click the ON button but nothing happens. The flashlight does not have batteries.");
        }
        }
        else if (position.equals("Kitchen") && LastLook.equals("south") && input.equals("use ladder") && inventory.contains("ladder"))
        {
            System.out.println("You have now reached the airway.");
            hasUsedLadder = true;
        }
        else if (position.equals("Kitchen") && LastLook.equals("south") && input.equals("use screwdriver"))
        {   if(hasUsedLadder){
            System.out.println("You have unscrewed the airway and can now open it.");
            hasUsedScrewdriver = true;}
            else{
            System.out.println("You can't open the airway, it's too high.");}
        }
        else if (input.equals("use keys")&&position.equals("Middle")&&inventory.contains("key1")&&inventory.contains("key2")){
            doorUnlocked=true;
            inventory.remove("key1");
            inventory.remove("key2");
            System.out.println("You unlocked the door successfully");
        }
        else
        {
            System.out.println("That's not a verb I can understand");
        }
    }

    public void Open(String input, String position, String LastLook) {
        int PIN= 0;

        if (position.equals("Dinning Room")&&LastLook.equals("east")&& input.equals("open drawers")){
            System.out.println("You just opened the drawers and found a photo of two passenger airplanes");}
        else if (position.equals("Dinning Room")&&LastLook.equals("south")&& input.equals("open safe")){
            System.out.println("You need a 3-digit PIN to open the safe \n" +
                    "PIN :");
            Scanner sc = new Scanner(System.in);
            PIN=sc.nextInt();
            if (PIN==911){
                System.out.println("You opened the safe. There is a key inside.");
                safeOpen = true;
            }
            else
            {
                System.out.println("WRONG PIN");}

        }
        else if (position.equals("Kitchen")&&LastLook.equals("south")&& input.equals("open airway") )
        { if(hasUsedScrewdriver){
                System.out.println("You have now opened the airway and can pick the key from inside");
                airwayOpen = true;}
            else{
            System.out.println("The airway needs to be unscrewed first in order to be opened.");}
        }
        else if (input.equals("open inventory")){
            System.out.println("Inventory : "+inventory);
        }
        else if (input.equals("open door")&& doorUnlocked){
            System.out.println("The door is opened.");
            doorOpen=true;
        }
        else
        {
            System.out.println("You can't open items without looking towards it first.");
        }
    }

    public void Pick(String input,String position, String LastLook)
    {
        if (inventory.size() < 6) {
            switch (position) {
                case "Supply Room":
                    if (input.equals("pick bat")&&LastLook.equals("north")) {
                        System.out.println("You have picked up a bat");
                        inventory.add("bat");
                        System.out.println("Inventory:" + inventory);
                    } else if (input.equals("pick shield")&&LastLook.equals("north")) {
                        System.out.println("You have picked up a shield");
                        inventory.add("shield");
                        System.out.println("Inventory:" + inventory);
                    } else if (input.equals("pick knives")&&LastLook.equals("west")) {
                        System.out.println("You have picked up the knives");
                        inventory.add("knives");
                        System.out.println("Inventory:" + inventory);
                    } else if (input.equals("pick Katana sword")&&LastLook.equals("west")) {
                        System.out.println("You have picked up a Katana sword");
                        inventory.add("Katana sword");
                        System.out.println("Inventory:" + inventory);
                    } else if (input.equals("pick arc")&&LastLook.equals("west")) {
                        System.out.println("You have picked up an arc");
                        inventory.add("arc");
                        System.out.println("Inventory:" + inventory);
                    } else if (input.equals("pick helmet")&&LastLook.equals("west")) {
                        System.out.println("You have picked up a helmet");
                        inventory.add("helmet");
                        System.out.println("Inventory:" + inventory);
                    } else if (input.equals("pick armor")&&LastLook.equals("west")) {
                        System.out.println("You have picked up an armor");
                        inventory.add("armor");
                        System.out.println("Inventory:" + inventory);
                    } else if (input.equals("pick potion")&&LastLook.equals("east")) {
                        System.out.println("You have picked up a potion");
                        inventory.add("potion");
                        System.out.println("Inventory:" + inventory);
                    }
                    else
                    {
                        System.out.println("You can't pick an item without look towards it first");
                    }
                    break;
                case "Middle":
                    System.out.println("There is nothing to pick here.");
                    break;
                case "Dinning Room":
                    if (input.equals("pick flashlight")&&LastLook.equals("west")) {
                        System.out.println("You have picked up a flashlight");
                        inventory.add("flashlight");
                        System.out.println("Inventory:" + inventory);
                    } else if ((input.equals("pick key")) && (safeOpen)&&LastLook.equals("south")) {
                        System.out.println("You have picked up a key");
                        inventory.add("key1");
                        System.out.println("Inventory:" + inventory);
                    } else {
                        System.out.println("You can't pick an item without looking towards it first.");
                    }
                    break;
                case "Kitchen":
                    if (input.equals("pick batteries")){
                        System.out.println("You have picked up batteries");
                        inventory.add("batteries");
                        System.out.println("Inventory:" + inventory);
                    } else if (input.equals("pick key") && (airwayOpen)) {
                        System.out.println("You have picked up a key");
                        inventory.add("key2");
                        System.out.println("Inventory:" + inventory);
                    } else {
                        System.out.println("That's not a sentence I can recognise.");
                    }
                    break;
                case "Warehouse":
                    if (input.equals("pick ladder")&&LastLook.equals("north")) {
                        System.out.println("You have picked up the ladder");
                        inventory.add("ladder");
                        System.out.println("Inventory:" + inventory);
                    }
                    else if (input.equals("pick screwdriver")&&LastLook.equals("south")) {
                        System.out.println("You have picked up a screwdriver");
                        inventory.add("screwdriver");
                        System.out.println("Inventory:" + inventory);
                    } else {
                        System.out.println("That's not a sentence I can recognise.");
                    }
                    break;
            }
        }
        else{
            System.out.println("Your inventory is full! You need to drop some items");
        }
    }

    public String Look(String input, String position, String playerName){

        switch (position){
            case "Middle":
                if (input.equals("look north")){
                    System.out.println("You see the locked Wooden Door.");}
                else if (input.equals("look west")){
                    System.out.println("You see the hallway leading to the Kitchen.");}
                else if (input.equals("look east")){
                    System.out.println("You see the hallway to stairs leading to the Warehouse.");
                }
                else if (input.equals("look south")) {
                    System.out.println("You see the hallway leading to the Dinning Room.");
                }
                else{
                    System.out.println("That's not a verb I can recognize.");
                }
                break;
            case "Dinning Room":
                if (input.equals("look north")){
                    LastLook="north";
                    System.out.println("You see the hallway leading to the Middle Room.");}
                else if (input.equals("look west")){
                    LastLook="west";
                    System.out.println("There is a nice, big dining table with some empty plates and glasses on it.\n " +
                            "Also you see a flashlight on the conrner of the table.");}
                else if (input.equals("look east")){
                    LastLook="east";
                    System.out.println("There is a domed mirror on a brick wall. Under the mirror there is a chest of three " +
                            "drawers.");}
                else if (input.equals("look south")){
                    LastLook="south";
                    System.out.println("You see some shelves on a wall. On the lower one there is a rusty safe.");}
                else {
                    System.out.println("That's not a verb I can recognize.");
                }
                break;
            case "Warehouse":
                if (input.equals("look north")){
                    LastLook="north";
                    System.out.println("You see a big, old wooden portable ladder that stands on the wall.");}
                else if (input.equals("look west")){
                    LastLook="west";
                    System.out.println("You see the stairs leading to the Middle Room.");}
                else if (input.equals("look east")){
                    LastLook="east";
                    System.out.println("There is a torn and dusty flag of America nailed to the wall.");}
                else if (input.equals("look south")){
                    LastLook="south";
                    System.out.println("You see a broken table with a rusty screwdriver on it.");}
                else {
                    System.out.println("That's not a verb I can recognize.");
                }
                break;
            case "Kitchen":
                if (input.equals("look north")){
                    LastLook="north";
                    System.out.println("There is a counter that has several dirty plates and some batteries on top of it.");}
                else if (input.equals("look west")){
                    LastLook="west";
                    System.out.println("You see a big white refrigerator and a photo of skyscrapers on it.");}
                else if (input.equals("look east")){
                    LastLook="east";
                    System.out.println("You see the hallway leading to the Middle Room.");}
                else if (input.equals("look south")){
                    LastLook="south";
                    System.out.println("There is an airway that has a key inside but it is too high and it is sealed. You need a ladder to reach it and a screwdriver to open it and grab the key");}
                else {
                    System.out.println("That's not a verb I can recognize.");
                }
                break;
            case "Supply Room":
                if (input.equals("look north")){
                    System.out.println("You see a red door in the middle of the wall.\n " +
                            "To the left side of the door there is a huge spiked bat(180 damage dealing) hanged.\n " +
                            "To the right side is a shiny round shield(20% damage taken reduction.)");
                LastLook="north";}
                else if (input.equals("look west")){
                    System.out.println("You see a photo frame of a woman you don't know.\n" +
                            "Under the picture there is a wooden table with the items below: \n" +
                            "A pair of little sharp knives. (115 damage)\n" +
                            "A Katana sword. (200 damage)\n" +
                            "An Arc with a bunch of arrows. (80 damage)\n" +
                            "A helmet. (-15% damage taken)\n" +
                            "An old armor (-15% damage taken)");
                LastLook="west";}
                else if (input.equals("look east")){
                    System.out.println("You see a TV screen on the wall. The TV screen displays the message below:\n\n" +
                            "Congratulations " +playerName+". Your intelligence was tested in the last three rooms \n" +
                            "but you managed to find all the required keys.\n" +
                            "BUT...now it's time to test the limits you willing to reach in order to survive.\n" +
                            "Behind the red door is a very good friend of mine who's really hungry.\n" +
                            "His favorite meal? Beautiful human beings like yourself.\n" +
                            "Arm yourself with anything you can find and carry, because you know\n" +
                            "my friend isn't that polite. HAHAAHAHA\n\n" +
                            "Also under the TV you see two small tables with an HP pot(+200HP)");

                LastLook="east";}
                else if (input.equals("look south")) {
                    System.out.println("You see the door leading to the Middle Room.");
                }
                else {
                    System.out.println("That's not a verb I can recognize.");
                }
                break;

            default:
                System.out.println("Not recognisable noun. Try something else.");
        }
        return LastLook;
    }
    public String Go(String input, String position){
        String randomEnemy;

        if(input.equals("go east"))
        {
            if (position.equals("Middle")) {
                if (flashlightAvailable)
                {
                    System.out.println("You just entered the Warehouse");                //Go east an eimaste sto Middle room//
                    position="Warehouse";
                }
                else
                {
                    System.out.println("It's too dark to wander in this room. You will need some source of lighting to continue.");
                }
            } else if (position.equals("Kitchen")) {
                System.out.println("You just entered the Middle room");       //Go east an eimaste sto Kitchen(west) room //
                position = "Middle";

            } else {
                System.out.println("NO EXIT");
            }
        }
        else if(input.equals("go west")) {

            if (position.equals("Middle")) {
                System.out.println("You just entered the Kitchen.");                //Go west an eimaste sto Middle room//
                position = "Kitchen";



            } else if (position.equals("Warehouse")) {
                System.out.println("You just entered the Middle room");       //Go west an eimaste sto Warehouse(west) room //
                position = "Middle";


            } else {
                System.out.println("NO EXIT");
            }
        }
        else if(input.equals("go north"))
        {
            if (position.equals("Middle"))
            {
                if (doorUnlocked && doorOpen)
                    {
                    System.out.println("You just entered the Supply Room");
                    position = "Supply Room";
                    }
                else
                {
                    System.out.println("The door is locked or not opened.");
                    position = "Middle";
                }
            }
            else if (position.equals("Supply Room"))
            {   //double dtr=0;
                double kappa;
                int enemyDmg;
                randomEnemy=getEnemy();
                System.out.println("You just entered the final room where you get to fight a "+randomEnemy+". You can use the items from the inventory, to help you survive.\n" +
                        "Attack by typing-->''attack with knives/arc/Katana sword'' etc.\n" +
                        "Gain 200HP by typing ''use health potion''.");
                position = "Fight Room";
                //enemyDmg=FightRoom(randomEnemy);
                //kappa= (enemyDmg-(enemyDmg*dmgTakenReduction(String.valueOf(inventory), dtr)));
                //System.out.println(kappa);

            }
            else if (position.equals("Dinning Room"))
            {
                System.out.println("You just entered the Middle room");
                position = "Middle";
            }
            else
            {
                System.out.println("NO EXIT");}
            }
        else if(input.equals("go south"))
        {
            if (position.equals("Middle"))
            {
                System.out.println("You just entered the Dining Room");
                position="Dinning Room";}
            else if (position.equals("Fight Room"))
            {
                System.out.println("You can't leave the Fight Room, the door is locked!");
                position="Fight Room";
            }
            else if (position.equals("Supply Room")){
                System.out.println("You just entered the Middle Room");
                position="Middle Room";}
            else{
                System.out.println("NO EXIT");}

        }
        else
        {
            System.out.println("That's not a verb I can recognize.");
        }
        return position;
    }

    public void Save(String position, String playerName){
        try{
            BufferedWriter bw = new BufferedWriter(new FileWriter("saveFile.txt"));
            bw.write(""+position);
            bw.newLine();
            bw.write(""+inventory.toString().replaceAll("[\\[\\](){}]", ""));
            bw.newLine();
            bw.write(""+safeOpen);
            bw.newLine();
            bw.write(""+airwayOpen);
            bw.newLine();
            bw.write(""+flashlightAvailable);
            bw.newLine();
            bw.write(""+hasUsedLadder);
            bw.newLine();
            bw.write(""+hasUsedScrewdriver);
            bw.newLine();
            bw.write(""+doorUnlocked);
            bw.newLine();
            bw.write(""+doorOpen);
            bw.newLine();
            bw.write(""+playerName);
            System.out.println("You successfully saved the game. ");


            bw.close();
        } catch (Exception e) {
        }

    }
    public String Load(String position) {
        try {
            BufferedReader br = new BufferedReader(new FileReader("saveFile.txt"));
            position = br.readLine();
            String inv1 = (br.readLine());
            String[] invParts = inv1.split(", ");
            List<String> listParts = Arrays.asList(invParts);
            inventory = new HashSet<String>( listParts );
            safeOpen = Boolean.parseBoolean(br.readLine());
            airwayOpen = Boolean.parseBoolean(br.readLine());
            flashlightAvailable = Boolean.parseBoolean(br.readLine());
            hasUsedLadder = Boolean.parseBoolean(br.readLine());
            hasUsedScrewdriver = Boolean.parseBoolean(br.readLine());
            doorUnlocked = Boolean.parseBoolean(br.readLine());
            doorOpen = Boolean.parseBoolean(br.readLine());
            String playerName = br.readLine();
            System.out.println("You successfully loaded the game.");
            br.close();

        } catch (Exception e) {
        }
        return position;
    }
    public static void Exit(){
        System.out.println("You exited the game.");
        System.exit(0);
    }

    public double Attack(String randomEnemy,String input,double kappa) throws InterruptedException {
        Scanner in2 = new Scanner(System.in);
        Scanner anns = new Scanner(System.in);
        double health = 800;
        int enemyhealth = 1000;
        double dtr=0;
        int enemyDmg;
        boolean alive = true;
        while (alive&&enemyhealth>0) {
            enemyDmg = FightRoom(randomEnemy);
            kappa = (enemyDmg - (enemyDmg * dmgTakenReduction(String.valueOf(inventory), dtr)));

            if (input.equals("attack with knives"))
            {
                if (inventory.contains("knives"))
                {
                    enemyhealth = enemyhealth - 115;
                    souts(enemyhealth, health,randomEnemy);
                    health = health - kappa;
                    Thread.sleep(2500);
                    souts2(randomEnemy, enemyhealth, health);
                }
                else{
                    System.out.println("You don't have knives in your inventory.");}
            }
            else if (input.equals("attack with katana"))
            {
                if(inventory.contains("Katana sword"))
                {
                    enemyhealth = enemyhealth - 200;
                    souts(enemyhealth,health,randomEnemy);
                    health=health-kappa;
                    Thread.sleep(2500);
                    souts2(randomEnemy,enemyhealth,health);
                }
                else{
                    System.out.println("You don't have Katana sword in your inventory.");}
            }

            else if (input.equals("attack with arc"))
            {
                if(inventory.contains("arc"))
                {
                    enemyhealth = enemyhealth - 80;
                    souts(enemyhealth,health,randomEnemy);
                    health=health-kappa;
                    Thread.sleep(2500);
                    souts2(randomEnemy,enemyhealth,health);
                }
                else{
                    System.out.println("You don't have arc in your inventory.");}
            }

            else if (input.equals("attack with bat"))
            {
                if(inventory.contains("bat"))
                {
                    enemyhealth = enemyhealth - 200;
                    souts(enemyhealth,health,randomEnemy);
                    health=health-kappa;
                    Thread.sleep(2500);
                    souts2(randomEnemy,enemyhealth,health);
                }
                else{
                    System.out.println("You don't have bat in your inventory.");}
            }
            else if (input.equals("exit")){
                System.out.println("If you exit the game now,the progress of the battle is going to be lost.\nAre you sure you want to exit? (yes/no)");
                String ans= anns.nextLine().trim();


                if(ans.equals("yes")){
                    Exit();
                }
                else{
                    souts(enemyhealth,health,randomEnemy);
                }
            }
            else if (input.equals("save game")){
                System.out.println("You can't save the game during the battle!");
            }
            else if (input.equals("use health potion")){
                if (inventory.contains("potion")){
                    health=health+200;
                    souts(enemyhealth,health,randomEnemy);
                }else{
                    System.out.println("You don't have a potion in your inventory.");
                }
            }
            else {
                System.out.println("Incorrect input.");
            }
            input  = in2.nextLine().trim();
        }
        return health;
    }
    public String getEnemy(){
        String[] enemies = {"ZOMBIE", "REPTILIAN", "VAMPIRE", "SKELETON"};
        Random rand = new Random();
        int z =rand.nextInt(enemies.length);
        String randomEnemy = enemies[z];
        return randomEnemy;
        }

    public double dmgTakenReduction(String inventory, double dtr) {
        if (inventory.contains("armor")){
            dtr= dtr + 0.15;
        }
        if (inventory.contains("Katana sword")){
            dtr = dtr + 0.1;
        }
        if (inventory.contains("helmet")){
            dtr= dtr + 0.15;
        }
        if (inventory.contains("shield")){
            dtr = dtr + 0.20;
        }

        return dtr;
    }

    public int FightRoom(String randomEnemy){
        int enemyDmg;

        if (randomEnemy.equals("REPTILIAN")){
            enemyDmg = 120;
        }
        else if (randomEnemy.equals("VAMPIRE")){
            enemyDmg = 200;
        }
        else if (randomEnemy.equals("SKELETON")){
            enemyDmg = 150;
        }
        else {
            enemyDmg = 105;
        }


        return enemyDmg;
    }
    public void souts(int enemyhealth,double health, String randomEnemy) throws InterruptedException {
        if (enemyhealth<0){
            enemyhealth=0;
            System.out.println(randomEnemy+ " HEALTH : "+enemyhealth);
            System.out.println("YOUR HEALTH :  "+health+"\n----------------------");
            Thread.sleep(1500);
            System.out.println("CONGRATULATIONS YOU WON THE BATTLE. THE "+randomEnemy+" IS DEAD!!!\n");
            System.out.println("YOU MANAGED TO SURVIVE AND NOW YOU ARE FREE TO LEAVE THIS GOD DAMN HOUSE!");
            System.out.println("Thanks for playing our game.");
            System.exit(0);
        }
        else{
        System.out.println(randomEnemy+ " HEALTH : "+enemyhealth);
        System.out.println("YOUR HEALTH :  "+health);
        System.out.println("----------------------\n----------------------");
    }}

    public void souts2(String randomEnemy,int enemyhealth,double health) throws InterruptedException {
        if (health<0){
            health=0;
            System.out.println("YOU WERE HIT BY THE "+randomEnemy);
            System.out.println(randomEnemy+ " HEALTH : "+enemyhealth);
            System.out.println("YOUR HEALTH : "+health+"\n----------------------");
            Thread.sleep(1500);
            System.out.println("YOU DIED!\n GAME OVER.");
            System.exit(0);
        }
        else{
        System.out.println("YOU WERE HIT BY THE "+randomEnemy);
        System.out.println(randomEnemy+ " HEALTH : "+enemyhealth);
        System.out.println("YOUR HEALTH : "+health);
    }}



}

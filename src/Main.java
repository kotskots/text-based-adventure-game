import java.util.*;


public class Main {
    public static void main(String[] args) throws InterruptedException {
        Scanner in = new Scanner(System.in);
        Commands User= new Commands();
        String position="Middle";
        String LastLook=" ";
        double kappa = 0;
        String randomEnemy = null;
        boolean running = true;
        double damageTakenReduction=0;
        String keys;

        //GAME:
        System.out.println("Welcome to the Zorks");
        System.out.println("--------------------------------");
        System.out.println("You just woke up from a deep sleep and you feel numb and dizzy. The only thing you can remember\n " +
                "is getting grabbed by someone just outside of your house and put in a car. You struggle to get up, but your arms and legs feel too weak.\n " +
                "You realise that you are in the middle of a big, empty room, lying on the cold floor, with a creepy white light bulb hanging above your head.  You see a tall, wooden door vaguely \n" +
                "about five meters north with two big locks on it. Also you can see three dark hallways: one on your right, one on your left and " +
                "one behind you. You better find a way to escape before it's too late.\n" +
                "------------------------------------------------------------------------\n" +
                "||| MOVE FROM ONE ROOM TO ANOTHER  --> go north/west/south/east.     |||\n" +
                "|||   PICK AN ITEM YOU SEE --> (pick key, pick screwdriver etc.)     |||\n" +
                "|||       OPEN SOMETHING   --> open safe, open airway etc.           |||\n" +
                "|||      LOOK AROUND YOU   --> look south/north/west/east            |||\n" +
                "|||      USE AN ITEM   --> use torch, use ladder etc.                |||\n" +
                "|||          SAVE/LOAD A GAME  --> save/load game                    |||\n" +
                "|||                EXIT THE GAME --> exit                            |||\n" +
                "------------------------------------------------------------------------\n");
        System.out.println("Please type your name.");
        String playerName = in.nextLine().trim();
        while (running) {
            System.out.print(">");
            String input = in.nextLine().trim();


            if (!input.contains("go") && !input.contains("look") && !input.contains("pick") && !input.contains("use") && !input.contains("drop") && !input.contains("open") && !input.equals("save game") && !input.equals("load game") && !input.equals("exit")&&!input.contains("attack"))
            {
                System.out.println("That's not a verb I can recognise.");
            }
            if (input.contains("go")) {
                position=User.Go(input,position);
            } else if (input.contains("look")) {
                LastLook=User.Look(input, position,playerName);
            }
            else if (input.contains("pick"))
            {
                User.Pick(input, position, LastLook);
            }
            else if (input.contains("open")){
                User.Open(input, position, LastLook);
            }
            else if (input.contains("use")){
                User.Use(input, position, LastLook);
            }
            else if (input.contains("drop")){
                User.Drop(input);
            }
            else if (input.equals("save game")){
                User.Save(position, playerName);
            }
            else if (input.equals("load game")){
                position = User.Load(position);
            }
            else if (input.equals("exit")){
                User.Exit();
            }
            else if (input.contains("attack")){
                if (position.equals("Fight Room"));
                    randomEnemy=User.getEnemy();
                    User.Attack(randomEnemy,input,damageTakenReduction);}
                else{
                System.out.println("There is nothing to attack here.");
                }
        }
    }
}
package pick_items;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

public class Commands {
    HashSet<String> inventory = new HashSet<>();

    public void Pick(String input, String position)
    {

        switch (position)
        {
            case "Middle":
                if (input.equals("pick key"))
                {
                    System.out.println("There is nothing to pick here");
                    System.out.println(inventory); // Αυτό μπήκε μόνο για να τεστάρω αν γεμίζει το HashSet
                }
                break;
            case "Dinning Room":
                if (input.equals("pick key"))
                {
                    System.out.println("You have picked up a key");
                    inventory.add("key1");
                }
                break;
            case "Kitchen":
                if (input.equals("pick key"))
                {
                    System.out.println("You have picked up a key");
                    inventory.add("key2");
                }
                break;
            case "Warehouse":
                if (input.equals("pick key"))
                {
                    System.out.println("You have picked up a key");
                    inventory.add("key3");
                }
                break;
        }
    }

    public String Look(String input, String position)
    {
        switch (position){
            case "Middle":
                if (input.equals("look north"))
                {
                    System.out.println("You see a Wooden Door with 3 locks.");
                }
                else if (input.equals("look west"))
                {
                    System.out.println("You see the hallway to Kitchen");
                }
                else if (input.equals("look east"))
                {
                    System.out.println("You see the hallway to Warehouse.");
                }
                else if (input.equals("look south"))
                {
                    System.out.println("You see the hallway to Dinning Room.");
                }
                break;
            case "Dinning Room":
                if (input.equals("look north"))
                {
                    System.out.println("You see the the door of the Dinning Room.");
                }
                else if (input.equals("look west"))
                {
                    System.out.println("You see the table with 6 chairs and a Key on it.");
                }
                else if (input.equals("look east"))
                {
                    System.out.println("You see 2 frames with several drawings.");
                }
                else if (input.equals("look south"))
                {
                    System.out.println("You see a large sofa.");
                }
                break;
            case "Warehouse":
                if (input.equals("look north"))
                {
                    System.out.println("You see an old table that has a Key on it.");
                }
                else if (input.equals("look west"))
                {
                    System.out.println("You see the the door of the Warehouse.");
                }
                else if (input.equals("look east"))
                {
                    System.out.println("You see a washing machine.");
                }
                else if (input.equals("look south"))
                {
                    System.out.println("You see a library full of old books.");
                }
                break;
            case "Kitchen":
                if (input.equals("look north"))
                {
                    System.out.println("You see a closet full of plates.");
                }
                else if (input.equals("look west"))
                {
                    System.out.println("You see a refrigerator. Above the refrigerator there is a Key");
                }
                else if (input.equals("look east"))
                {
                    System.out.println("You see the the door of the Kitchen.");
                }
                else if (input.equals("look south"))
                {
                    System.out.println("You see a sink with an oven.");
                }
                break;

            default:
                System.out.println("Not recognisable noun. Try something else.");
        }
        return null;
    }



    public String Go(String input, String position,int num_Moves,int score)
    {
        if(input.equals("go east"))
        {
            if (position.equals("Middle"))
            {
                System.out.println("You just entered the Warehouse.");                //Go east an eimaste sto Middle room//
                position="Warehouse";
                System.out.println("Moves:" + num_Moves + "\nScore:" + score + "");
                num_Moves++;
            }
            else if (position.equals("Kitchen"))
            {
                System.out.println("You just entered the Middle room");       //Go east an eimaste sto Kitchen(west) room //
                position = "Middle";
                System.out.println("Moves:" + num_Moves + "\nScore:" + score + "");
                num_Moves++;
            }
            else
            {
                System.out.println("NO EXIT");
            }
        }
        else if(input.equals("go west"))
        {
            if (position.equals("Middle"))
            {
                num_Moves++;
                System.out.println("You just entered the Kitchen.");                //Go west an eimaste sto Middle room//
                position = "Kitchen";
                System.out.println("Moves:" + num_Moves + "\nScore:" + score + "");
            }
            else if (position.equals("Warehouse"))
            {
                System.out.println("You just entered the Middle room");       //Go west an eimaste sto Warehouse(west) room //
                position = "Middle";
                System.out.println("Moves:" + num_Moves + "\nScore:" + score + "");
                num_Moves++;
            }
            else
            {
                System.out.println("NO EXIT");
            }
        }
        else if(input.equals("go north"))
        {
            if (position.equals("Middle"))
            {
                if (inventory.contains("key1") && (inventory.contains("key2") && inventory.contains("key3")))
                {
                    System.out.println("You won da game"); // Αυτό μπήκε μόνο για να δω αν τα κλειδιά αλληλεπηδρούν με επιλογές στο game
                }
                else
                {
                    num_Moves++;
                    System.out.println("The door is Locked.");                  //go north an eimaste sto Middle room//
                    position = "Middle";
                    System.out.println("Moves:" + num_Moves + "\nScore:" + score + "");
                }
            }
            else if (position=="Dinning Room")
            {
                System.out.println("You just entered the Middle room");
                num_Moves++;
                System.out.println("Moves:" + num_Moves + "\nScore:" + score + "");     //go north an eimaste sto Dinning Room//
                position = "Middle";
            }
            else
            {
                System.out.println("NO EXIT");}
        }
        else if(input.equals("go south"))
        {
            if (position.equals("Middle"))
            {
                num_Moves++;
                System.out.println("You just entered the Dinning Room");                //go south otan eimaste sto Middle room//
                position="Dinning Room";
                System.out.println("Moves:"+num_Moves+"\nScore:"+score+"");
            }
            else if (position.equals("North"))
            {
                num_Moves++;
                System.out.println("You just entered the North Room");          //akoma den exoyme pei ti fasi me to room sto North ..na to koitajoume//
                position="North Room";
                System.out.println("Moves:"+num_Moves+"\nScore:"+score+"");
            }
            else
            {
                System.out.println("NO EXIT");
            }
        }
        return position;
    }
}